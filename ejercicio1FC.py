#!/usr/bin/env python
# -*- coding: utf-8 -*-
#EJERCICIO 1.A
#Se define la variable suma
def suma_numeros(numero_lista):
	suma = sum(numero_lista)
	return suma
#Se define la variable largo lista 
def largo_lista(numero_lista):
	largo = len(numero_lista)
	return largo
#se dan los valores de la lista 
numero_lista = [2.3, 4.7, 5.7, 8.9]
print ("Los numeros de la lista son:", numero_lista)
#se llaman a las variables 
suma_final = suma_numeros(numero_lista)
largo_final = largo_lista(numero_lista)
#Se calcula el promedio 
promedio = suma_final / largo_final
print("El promedio de los numeros sera de:", promedio)

#EJERCICIO 1. B
#Se define la funcion contador de letras 
def contador_letras(frase):
	frase.sort()
	return frase[len(frase) - 1]
#se ingresa la frase
frase = ['color', 'morado', 'azul', 'cian']
print ("La frase con mayor cantidad de letras es: ", contador_letras(frase) ) 

#EJERCICIO 1.C 
#se define la variable cuadrado
numero = [2,4,5,8]
elevado = []
elevar=0
i = 0
def cuadrado (numero):
	elevado = []
	elevar=0
	i = 0
	a=0
	for i in numero:
		elevar = i
		a=elevar*elevar
		elevado.append( a)
	return elevado
#Se entregan los valores del cuadrado


print ("Los numeros cuadrados son: ", cuadrado(numero))




